export class registro {
  constructor() {}

  etiquetas() {
    let espa = {
      errorPass1:
        'Contraseña es obligatoria y debe cumplir con los criterios de seguridad (mínimo 8 caracteres, 1 mayúscula, 1 minúscula, 1 número, 1 caracter especial !@#$%^&*(),.?":{}|<>)',
      nombre: 'Nombre',
      nombreEsObligatorio: 'Nombre es obligatorio',
      apellidoPaterno: 'Apellido paterno',
      apellidoMaterno: 'Apellido materno',
      apellidoPaternoOblitario: 'Apellido paterno es obligatorio',
      apellidoMaternoOnligatorio: 'Apellido materno es obligatorio',
      telefono: 'Teléfono',
      fechaNacimiento: 'Fecha de nacimiento',
      fechaNacimientoObligatorio: 'Fecha de nacimiento es obligatorio',
      correoElectronico: 'Correo electronico',
      noCedulaProfecional: 'Número de cedula profecional',
      cedulaObligatoria: 'Cédula Profesional es obligatorio',
      archivoCelula: 'Archivo de la cédula',
      archivoObligatorio: 'Archivo de la cédula es obligatorio',
      estado: 'Estado',
      estadoObligatorio: 'Estado obligatorio',
      contrasenia: 'Contraseña',
      verificacionContrasenia: 'Verificación de contraseña',
      verificarContrasenia:
        'La contraseña es obligatoria y debe coincidir con la contraseña',
      contraseniaNoCoinciden: 'Las contraseñas no coinciden',
      registrar: 'Registrar',
      validaCorreo: 'Correo electrónico es obligatorio',
      validaArchivoPDF: 'El archivo debe ser de tipo PDF',
    };
    return espa;
  }
}
