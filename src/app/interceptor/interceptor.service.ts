import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { EncryptionService } from '../encryption/encryption.service';

@Injectable({
  providedIn: 'root',
})
export class InterceptorService implements HttpInterceptor {
  constructor(private encryptionService: EncryptionService) {}
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const token = localStorage.getItem('authToken'); // Recupera el token desde localStorage

    /**

*/

    this.encryptWord();

    if (token) {
      // Clona la solicitud para agregar la nueva cabecera
      const clonedRequest = req.clone({
        setHeaders: {
          Authorization: `Bearer ${token}`,
        },
      });
      console.log('peticion con token');
      // Enviar la solicitud clonada con el token
      return next.handle(clonedRequest);
    } else {
      // Enviar la solicitud original si no hay token
      console.log('oeticion normal');
      return next.handle(req);
    }
  }

  encryptWord(): void {
    let encryptedWord = this.encryptionService.encrypt('ya quedo');
    console.log('Encrypted Word:', encryptedWord);
    this.decryptWord(encryptedWord);
  }

  decryptWord(fraseEncriptada: string): void {
    let decryptedWord = this.encryptionService.decrypt(fraseEncriptada);
    console.log('Decrypted Word:', decryptedWord);
  }
}
