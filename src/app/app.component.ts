import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { InactivityService } from './inactivity/inactivity.service';
import { RouterOutlet } from '@angular/router';
import { SpinnerComponent } from './spinner/spinner.component';
import { CommonModule } from '@angular/common';
import { ToastModule } from 'primeng/toast';

@Component({
  selector: 'app-root',
  standalone: true,
  templateUrl: './app.component.html',
  styleUrl: './app.component.css',
  imports: [RouterOutlet, CommonModule, SpinnerComponent,ToastModule],
})
export class AppComponent implements OnInit {
  title = 'doctoralandia';
  @ViewChild('inactivityDialogContainer', {
    read: ViewContainerRef,
    static: true,
  })
  viewContainerRef!: ViewContainerRef;

  constructor(private inactivityService: InactivityService) {}

  ngOnInit(): void {
    this.inactivityService.setViewContainerRef(this.viewContainerRef);
  }
}
