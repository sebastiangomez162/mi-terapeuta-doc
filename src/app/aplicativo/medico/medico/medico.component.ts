import { Component } from '@angular/core';
import { MenuComponent } from "../../menu/menu.component";

@Component({
    selector: 'app-medico',
    standalone: true,
    templateUrl: './medico.component.html',
    styleUrl: './medico.component.css',
    imports: [MenuComponent]
})
export class MedicoComponent {

}
