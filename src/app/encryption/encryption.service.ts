import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';
@Injectable({
  providedIn: 'root',
})
export class EncryptionService {
  private secretKey = 'mipalabraSecreta'; // Cambiar esto por una clave secreta segura

  constructor() {}

  encrypt(word: string): string {
    return CryptoJS.AES.encrypt(word, this.secretKey).toString();
  }

  decrypt(encryptedWord: string): string {
    const bytes = CryptoJS.AES.decrypt(encryptedWord, this.secretKey);
    return bytes.toString(CryptoJS.enc.Utf8);
  }
}
