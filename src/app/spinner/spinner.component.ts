import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { SpinnerService } from './spinner.service';

@Component({
  selector: 'app-spinner',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './spinner.component.html',
  styleUrl: './spinner.component.css',
})
export class SpinnerComponent implements OnInit {
  spinnerVisible$: Observable<boolean>;

  constructor(private spinnerService: SpinnerService) {
    this.spinnerVisible$ = this.spinnerService.spinner$;
  }

  ngOnInit(): void {}
}
