import { Component } from '@angular/core';
import { DialogModule } from 'primeng/dialog';
@Component({
  selector: 'app-inactivity-dialog',
  standalone: true,
  imports: [DialogModule],
  templateUrl: './inactivity-dialog.component.html',
  styleUrl: './inactivity-dialog.component.css',
})
export class InactivityDialogComponent {
  display: boolean = false;

  closeDialog(): void {
    this.display = false;
  }
}
