import {
  Injectable,
  NgZone,
  ViewContainerRef,
  ComponentRef,
} from '@angular/core';
import { InactivityDialogComponent } from './inactivity-dialog/inactivity-dialog.component';

@Injectable({
  providedIn: 'root',
})
export class InactivityService {
  private timeoutId: any;
  private readonly timeout: number = 1 * 60 * 1000; // 20 minutos en milisegundos
  private dialogComponentRef: ComponentRef<InactivityDialogComponent> | null =
    null;
  private viewContainerRef!: ViewContainerRef;

  constructor(private ngZone: NgZone) {
    this.startTimer();
    this.setupEventListeners();
  }

  setViewContainerRef(vcr: ViewContainerRef): void {
    this.viewContainerRef = vcr;
  }

  private startTimer(): void {
    this.ngZone.runOutsideAngular(() => {
      this.resetTimer();
    });
  }

  private resetTimer(): void {
    clearTimeout(this.timeoutId);
    this.timeoutId = setTimeout(() => this.handleInactivity(), this.timeout);
  }

  private handleInactivity(): void {
    this.ngZone.run(() => {
      this.showInactivityDialog();
    });
  }

  private showInactivityDialog(): void {
    if (!this.dialogComponentRef) {
      this.dialogComponentRef = this.viewContainerRef.createComponent(
        InactivityDialogComponent
      );
    }
    console.log('muestra el dialogo');
    const token = localStorage.getItem('authToken');
    if (token) this.dialogComponentRef.instance.display = true;
  }

  private setupEventListeners(): void {
    const events = ['mousemove', 'click', 'keydown', 'scroll', 'touchstart'];

    events.forEach((event) => {
      window.addEventListener(event, () => this.resetTimer(), true);
    });
  }
}
