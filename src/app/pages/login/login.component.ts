import { Component, OnInit } from '@angular/core';

import { ButtonModule } from 'primeng/button';
import { PasswordModule } from 'primeng/password';
import { MessageService } from 'primeng/api';
import { LoginService } from '../services/login.service';
import { HttpErrorResponse } from '@angular/common/http';
import {
  FormGroup,
  FormControl,
  FormsModule,
  ReactiveFormsModule,
} from '@angular/forms';
import { SpinnerService } from '../../spinner/spinner.service';
import { CommonModule } from '@angular/common';
import { finalize, takeUntil } from 'rxjs/operators';
import { EncryptionService } from '../../encryption/encryption.service';
import { GeneralesService } from '../../funciones/generales.service';
import { Subject } from 'rxjs';
import { login } from '../../i18n/en/login';
@Component({
  selector: 'app-login',
  standalone: true,
  imports: [
    FormsModule,
    ReactiveFormsModule,
    ButtonModule,
    PasswordModule,
    CommonModule,
  ],
  templateUrl: './login.component.html',
  styleUrl: './login.component.css',
})
export class LoginComponent implements OnInit {
  etiquetas = new login();
  tags = this.etiquetas.etiquetas();

  data: any;
  private unsubscribe$ = new Subject<void>();
  constructor(
    private service: LoginService,
    private spinnerService: SpinnerService,
    private encryptionService: EncryptionService,
    private funcionesGenerales: GeneralesService,
    private messageService: MessageService
  ) {}
  ngOnInit(): void {
    this.spinnerService.hide();
    const valor =
      localStorage.getItem('language') == null
        ? 'en'
        : localStorage.getItem('language');
  }
  loginForm = new FormGroup({
    email: new FormControl(''),
    password: new FormControl(''),
    action: new FormControl('login'),
  });

  onSubmit(): void {
    this.spinnerService.show();
    this.service
      .login(this.loginForm.value, 'login')
      .pipe(
        finalize(() => {
          this.spinnerService.hide(); // Esto se ejecutará al completar o fallar
        })
      )
      .subscribe({
        next: (respuesta: any) => {
          console.log(respuesta);
          let token = {
            token: respuesta.datos,
            validacion: 'ok',
          };
          let encryptedWord = this.encryptionService.encrypt(token.toString());
          this.funcionesGenerales.setearItem(encryptedWord, 'authToken');
          alert(respuesta.datos);
        },
        error: (error: HttpErrorResponse): void => {
          console.log(error);
          let token = '{"hola":"sadasda","adios":"asdasdasdad"}';
          let encryptedWord = this.encryptionService.encrypt(token);
          console.log(encryptedWord);
          let desencriptar = this.encryptionService.decrypt(encryptedWord);
          let datos = this.funcionesGenerales.parseJson(desencriptar);
          console.log(datos['hola']);

          this.funcionesGenerales.validarCodigoError(error);
        },
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
