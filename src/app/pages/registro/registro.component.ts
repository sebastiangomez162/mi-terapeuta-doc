import { Component, OnInit } from '@angular/core';
import { LoginService } from '../services/login.service';
import { HttpErrorResponse } from '@angular/common/http';

import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import {
  FormBuilder,
  FormGroup,
  Validators,
  AbstractControl,
  ValidationErrors,
  AbstractControlOptions,
} from '@angular/forms';

import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule, DatePipe } from '@angular/common';
import { ToastModule } from 'primeng/toast';
import { registro } from '../../i18n/es/registro';
import { ActivatedRoute, Router } from '@angular/router';
import { CalendarModule } from 'primeng/calendar';
import { SpinnerService } from '../../spinner/spinner.service';
import { GeneralesService } from '../../funciones/generales.service';
import { finalize } from 'rxjs';
import { DropdownModule } from 'primeng/dropdown';
import { selectGenerico } from '../../interface/selectGenerico';
import { estados } from '../../interface/estados';
import { PasswordModule } from 'primeng/password';
@Component({
  selector: 'app-registro',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    InputTextModule,
    ButtonModule,
    ToastModule,
    CalendarModule,
    DropdownModule,
    PasswordModule,
  ],
  providers: [DatePipe],
  templateUrl: './registro.component.html',
  styleUrl: './registro.component.css',
})
export class RegistroComponent implements OnInit {
  estados: selectGenerico[] = estados;

  etiquetas = new registro();
  tags = this.etiquetas.etiquetas();
  muestraform: boolean = false;
  fileError: string | null = null;
  registrationForm!: FormGroup;

  constructor(
    private service: LoginService,
    private spinnerService: SpinnerService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private funcionesGenerales: GeneralesService,
    private datePipe: DatePipe
  ) {}

  ngOnInit(): void {
    let url = this.router.url.split('/')[2].toString();
    const valor =
      localStorage.getItem('language') == null
        ? 'es'
        : localStorage.getItem('language');
    if (url == 'registro-medico') {
      this.muestraform = true;
      this.registrationForm = this.fb.group(
        {
          nombre: ['', Validators.required],
          paterno: ['', Validators.required],
          materno: ['', Validators.required],
          fechaNacimiento: ['', Validators.required],
          telefono: [''],
          email: ['', [Validators.required, Validators.email]],
          numCedula: ['', Validators.required],
          idEstado: [{ estados: ['', Validators.required] }],
          documentoCedula: [''],
          tipoPersona: [2],
          professionalIdFile: [null, Validators.required],
          password: [
            '',
            [
              Validators.required,
              Validators.minLength(8),
              this.passwordValidator,
            ],
          ],
          confirmPassword: ['', Validators.required],
        },
        { validator: this.passwordMatchValidator } as AbstractControlOptions
      );
    }
  }
  passwordValidator(control: AbstractControl): ValidationErrors | null {
    const value = control.value;
    if (!value) return null;

    const hasUpperCase = /[A-Z]/.test(value);
    const hasLowerCase = /[a-z]/.test(value);
    const hasNumeric = /[0-9]/.test(value);
    const hasSpecialChar = /[!@#$%^&*(),.?":{}|<>]/.test(value);

    const valid = hasUpperCase && hasLowerCase && hasNumeric && hasSpecialChar;
    if (!valid) {
      return { passwordInvalid: true };
    }
    return null;
  }

  passwordMatchValidator(form: AbstractControl) {
    const password = form.get('password')!.value;
    const confirmPassword = form.get('confirmPassword')!.value;
    if (password !== confirmPassword) {
      return { mismatch: true };
    }
    return null;
  }

  onFileChange(event: any) {
    const file: File = event.target.files[0];
    if (file) {
      if (file.type === 'application/pdf') {
        const reader = new FileReader();
        reader.onload = () => {
          this.registrationForm.patchValue({
            professionalIdFile: 'xs',
          });

          this.registrationForm.patchValue({
            documentoCedula: (reader.result as string).split(',')[1],
          });
          this.fileError = null;
        };

        reader.readAsDataURL(file);
      } else {
        this.registrationForm.patchValue({
          documentoCedula: null,
        });
        this.fileError = this.tags.validaArchivoPDF;
      }
    }
  }
  onSubmit() {
    if (this.registrationForm.valid) {
      const formValues = this.registrationForm.value;
      const formattedDate = this.datePipe.transform(
        formValues.fechaNacimiento,
        'yyyy-MM-dd'
      );
      formValues.fechaNacimiento = formattedDate;
      this.spinnerService.show();
      this.service
        .registrarUsuario(this.registrationForm.value, 'registro')
        .pipe(
          finalize(() => {
            this.spinnerService.hide(); // Esto se ejecutará al completar o fallar
          })
        )
        .subscribe({
          next: (respuesta: any) => {
            alert(respuesta.datos);
          },
          error: (error: HttpErrorResponse): void => {
            this.funcionesGenerales.validarCodigoError(error);
          },
        });
    } else {
      console.log('Form is invalid' + this.registrationForm.value);
    }
  }
}
