import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  Host = 'https://miterapeuta.com.mx/api/auth/';
  constructor(private http: HttpClient) {}

  login(datos: any, url: string): Observable<any> {
    return this.http.post<any>(this.Host + url, datos).pipe(
      catchError((error: HttpErrorResponse) => {
        console.error('Error en la petición:', error);
        return throwError(() => error);
      })
    );
  }

  registrarUsuario(datos: any, url: string): Observable<any> {
    return this.http.post<any>(this.Host + url, datos).pipe(
      catchError((error: HttpErrorResponse) => {
        console.error('Error en la petición:', error);
        return throwError(() => error);
      })
    );
  }
}
