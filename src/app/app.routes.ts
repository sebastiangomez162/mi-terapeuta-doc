import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { RegistroComponent } from './pages/registro/registro.component';
import { RecuperarContraseniaComponent } from './pages/recuperar-contrasenia/recuperar-contrasenia.component';
import { MedicoComponent } from './aplicativo/medico/medico/medico.component';
import { guardGuard } from './guard/guard.guard';
export const routes: Routes = [
  {
    path: 'pages',
    children: [
      { path: 'login', component: LoginComponent },
      { path: 'recuperar-password', component: RecuperarContraseniaComponent },
      { path: 'registro-medico', component: RegistroComponent },
      { path: 'registro-paciente', component: RegistroComponent },
      { path: '**', component: LoginComponent },
    ],
  },
  {
    path: 'aplicativo',
    canActivate: [guardGuard],
    children: [
      { path: 'medico', component: MedicoComponent, canActivate: [guardGuard] },
      { path: '**', component: LoginComponent },
    ],
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      enableTracing: false, // <-- debugging purposes only
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
