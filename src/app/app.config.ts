import { ApplicationConfig } from '@angular/core';
import { provideRouter } from '@angular/router';
import { provideAnimations } from '@angular/platform-browser/animations';
import { routes } from './app.routes';
import { LoginService } from './pages/services/login.service';
import {
  HTTP_INTERCEPTORS,
  provideHttpClient,
  withFetch,
  withInterceptorsFromDi,
} from '@angular/common/http';
import { InterceptorService } from './interceptor/interceptor.service';
import { InactivityDialogComponent } from './inactivity/inactivity-dialog/inactivity-dialog.component';
import { MessageService } from 'primeng/api';
import { ToastModule } from 'primeng/toast';
export const appConfig: ApplicationConfig = {
  providers: [
    provideRouter(routes),
    provideAnimations(),
    LoginService,
    InactivityDialogComponent,
    MessageService,
    ToastModule,
    // provideHttpClient(withFetch()),
    provideHttpClient(withInterceptorsFromDi()),
    { provide: HTTP_INTERCEPTORS, useClass: InterceptorService, multi: true },
  ],
};
