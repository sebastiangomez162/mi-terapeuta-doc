import { CanActivateFn } from '@angular/router';
import { inject } from '@angular/core';
import { Router } from '@angular/router';
export const guardGuard: CanActivateFn = (route, state) => {
  const router = inject(Router);
  const token = localStorage.getItem('authToken');

  if (token) {
    return true;
  } else {
    // Redirige al usuario a la página de inicio de sesión
    return router.createUrlTree(['pages/login']);
  }
};
