export function validarCURP(curp: string): boolean {
  const curpRegex =
    /^([A-Z]{4})(\d{6})([H,M])([A-Z]{2})([A-Z]{3})([A-Z\d]{1})(\d{1})$/;

  if (!curpRegex.test(curp)) {
    return false;
  }

  const match = curp.match(curpRegex);
  if (!match) {
    return false;
  }

  const year = parseInt(match[2].substring(0, 2), 10);
  const month = parseInt(match[2].substring(2, 4), 10);
  const day = parseInt(match[2].substring(4, 6), 10);
  const dateValid = validateDate(year, month, day);

  return dateValid;
}

function validateDate(year: number, month: number, day: number): boolean {
  if (month < 1 || month > 12) {
    return false;
  }

  const leapYear = (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;
  const daysInMonth = [
    31,
    leapYear ? 29 : 28,
    31,
    30,
    31,
    30,
    31,
    31,
    30,
    31,
    30,
    31,
  ];

  return day > 0 && day <= daysInMonth[month - 1];
}

export function validarRFC(rfc: string): boolean {
  const rfcRegex = /^([A-ZÑ&]{3,4})(\d{2})(\d{2})(\d{2})([A-Z\d]{3})$/;

  if (!rfcRegex.test(rfc)) {
    return false;
  }

  const match = rfc.match(rfcRegex);
  if (!match) {
    return false;
  }

  const year = parseInt(match[2], 10);
  const month = parseInt(match[3], 10);
  const day = parseInt(match[4], 10);
  const dateValid = validateDate(year, month, day);

  return dateValid;
}
