import { Injectable } from '@angular/core';
import { validarCURP, validarRFC } from './utils.js';
import { HttpErrorResponse } from '@angular/common/http/index.js';
import { MessageService } from 'primeng/api';
@Injectable({
  providedIn: 'root',
})
export class GeneralesService {
  constructor(private messageService: MessageService) {}
  isCurpValid: boolean = false;
  isRfcValid: boolean = false;

  parseJson(jsonString: string): any {
    try {
      let jsonObject: JSON;
      jsonObject = JSON.parse(jsonString);
      console.log('conversion a objeto:', jsonObject);
      return jsonObject;
    } catch (error) {
      console.error('Invalid JSON string', error);
      return null;
    }
  }

  validaCURP(curp: string): boolean {
    return validarCURP(curp);
  }

  validaRFC(rfc: string): boolean {
    return validarRFC(rfc);
  }

  // Método para guardar el token en localStorage
  setearItem(token: string, tokenKey: string): void {
    localStorage.setItem(tokenKey, token);
  }

  // Método para obtener el token de localStorage
  obtenerItem(tokenKey: string): string | null {
    return localStorage.getItem(tokenKey);
  }

  // Método para eliminar el token de localStorage
  removerItem(tokenKey: string): void {
    localStorage.removeItem(tokenKey);
  }

  validarCodigoError(error: HttpErrorResponse): void {
    if (error.status === 0) {
      // A network error occurred. Handle accordingly.
      console.error('Network error:', error);
      this.showMessage(
        'error',
        'Error',
        'Error de red. Por favor verifique su conexión.'
        
      );
    } else if (error.status >= 400 && error.status < 500) {
      debugger
      // Handle client-side errors (4xx)
      console.error('Client error:', error);
      this.showMessage('error',
        'Cliente error',
        `Client error: ${error.error.message || error.message}`,
      );
    } else if (error.status >= 500) {
      // Handle server-side errors (5xx)
      console.error('Server error:', error);
      this.showMessage(
        'error',
        'Error',
        'Error del Servidor. Por favor, inténtelo de nuevo más tarde.',
        
      );
    } else {
      // Handle other types of errors
      console.error('Unexpected error:', error);
      this.showMessage(
        'error',
        'Error',
        'Ocurrió un error inesperado. Inténtalo de nuevo.',
        
      );
    }
  }

  showMessage( severity: string = 'info',summary: string, detail: string = 'detalle'): void {
    /*los valores que puede tomar son 
    success: correcto,
    info:informativo,
    primary: texto con negritas y fondo blanco,
    warn: es  como naranja,
    error: para errores y es de color rojo,
    contrast: mensaje con fondo negro y letras blancas
    */

    //TODO anexar detalles a estructura de mensaje

    this.messageService.add({ severity: severity, summary: summary, detail: detail });
  }
}
